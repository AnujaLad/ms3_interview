1. Please execute Sql script from DDL-SQL folder. 
2. To run a job, run App.java from com.ms3.csvutils
3. Wait till job completion for execution, after step: [step1] (logging has to be added)
4. Job reads Csv input file from resources/input folder. To replace this file, use this path.
5. Bad data and record stats are written to output folder inside parent project folder.