package com.ms3.csvutils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Entry point for batch start
 *
 *
 */
public class App {
	private static final String FILENAME = "output\\record-log.txt";

	private static Iterator<StepExecution> ex;

	public static void main(String[] args) {
		BufferedWriter buffer = null;
		FileWriter file = null;

		String[] springConfig = { "configuration/spring-batch-job.xml" };

		ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);

		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		Job job = (Job) context.getBean("csvJob");

		try {

			JobExecution execution = jobLauncher.run(job, new JobParameters());
			System.out.println("Exit Status : " + execution.getStatus());
			int skippedReadCount = 0;
			int readCount = 0;
			int totalCount = 0;
			Iterator<StepExecution> ex = execution.getStepExecutions().iterator();
			if (ex.hasNext()) {
				StepExecution stepExecution = ex.next();
				readCount = stepExecution.getReadCount();
				skippedReadCount = stepExecution.getReadSkipCount();
			}
			totalCount = skippedReadCount + readCount;

			file = new FileWriter(FILENAME);
			buffer = new BufferedWriter(file);
			buffer.write(" 1. # of records received : " + totalCount + "\n");
			buffer.write(" 2. # of records successful : " + readCount + "\n");
			buffer.write(" 3. # of records failed : " + skippedReadCount + "\n");

			System.out.println("Done");

		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (buffer != null)
					buffer.close();

				if (file != null)
					file.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}

		}
	}

}
