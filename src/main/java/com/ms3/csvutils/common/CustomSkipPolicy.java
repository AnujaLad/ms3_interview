package com.ms3.csvutils.common;

import java.io.FileNotFoundException;
import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;
import org.springframework.batch.item.file.FlatFileParseException;

/**
 * To allow skipping a bad read
 *
 */
public class CustomSkipPolicy implements SkipPolicy {

	@Override
	public boolean shouldSkip(Throwable exception, int skipCount) throws SkipLimitExceededException {
		if (exception instanceof FileNotFoundException) {
			return false;
		} else if (exception instanceof FlatFileParseException) {
			// logging
			return true;

		} else {
			return false;
		}
	}

}
