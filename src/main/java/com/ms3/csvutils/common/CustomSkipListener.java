package com.ms3.csvutils.common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.batch.core.SkipListener;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.ms3.csvutils.model.Record;

/**
 * Listener to write skipped records
 *
 *
 */
public class CustomSkipListener implements SkipListener<Record, Throwable> {

	FlatFileItemWriter<String> writer;

	public FlatFileItemWriter<String> getWriter() {
		return writer;
	}

	public void setWriter(FlatFileItemWriter<String> writer) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_HHmmss");
		String strDate = sdf.format(Calendar.getInstance().getTime());
		writer.setResource((Resource) new FileSystemResource("output/bad-data-" + strDate + ".csv"));
		this.writer = writer;
	}

	@Override
	public void onSkipInRead(Throwable arg0) {
		try {
			if (arg0 instanceof FlatFileParseException) {
				FlatFileParseException ffpe = (FlatFileParseException) arg0;
				List<String> lines = new ArrayList<String>();
				lines.add(ffpe.getInput());
				writer.write(lines);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	@Override
	public void onSkipInWrite(Throwable arg0, Throwable arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSkipInProcess(Record arg0, Throwable arg1) {
		// TODO Auto-generated method stub

	}

}
