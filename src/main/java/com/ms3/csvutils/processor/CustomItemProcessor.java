package com.ms3.csvutils.processor;

import org.springframework.batch.item.ItemProcessor;

import com.ms3.csvutils.model.Record;

/**
 * Processes a record
 *
 *
 */
public class CustomItemProcessor implements ItemProcessor<Record, Record> {

	@Override
	public Record process(Record item) throws Exception {
		// processing and logging
		
		return item;
	}

}
