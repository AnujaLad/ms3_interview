package com.ms3.csvutils.reader;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.ms3.csvutils.model.Record;

/**
 * Mapping a record to an object by index#
 *
 *
 */
public class CustomFieldSetMapper implements FieldSetMapper<Record> {

	@Override
	public Record mapFieldSet(FieldSet fieldSet) throws BindException {

		Record record = new Record();

		record.setA(fieldSet.readString(0));
		record.setB(fieldSet.readString(1));
		record.setC(fieldSet.readString(2));
		record.setD(fieldSet.readString(3));
		record.setE(fieldSet.readString(4));
		record.setF(fieldSet.readString(5));
		record.setG(fieldSet.readString(6));
		record.setH(fieldSet.readString(7));
		record.setI(fieldSet.readString(8));
		record.setJ(fieldSet.readString(9));

		return record;

	}

}
