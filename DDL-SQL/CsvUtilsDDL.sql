CREATE DATABASE csv_utils;
COMMIT;

CREATE TABLE csv_utils.`record` (
  `A` varchar(50) DEFAULT NULL,
  `B` varchar(50) DEFAULT NULL,
  `C` varchar(50) DEFAULT NULL,
  `D` varchar(50) DEFAULT NULL,
  `E` text,
  `F` varchar(50) DEFAULT NULL,
  `G` varchar(50) DEFAULT NULL,
  `H` varchar(50) DEFAULT NULL,
  `I` varchar(50) DEFAULT NULL,
  `J` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;